# Print first 10 natural number using while loop
i = 1
while i <= 10:
    print(i)
    i = i+1

# Print in reverse
i = 10
while i >= 1:
    print(i)
    i = i-1

# Print from -10 to -1 using for loop
for i in range(-10, 0):
    print(i)

# Write a program to display all even numbers within a range
for i in range(0, 10, 2):
    print(i)

# count the number of digit in a number.
lists = int(input("Enter the list:"))
count = 0
while (lists> 0):
    count = count+1
    lists=lists//10
print("Number of digit in list is ", count)

# Multiplication table program
num = int(input("Enter the number:"))
for i in range(1, 11):
    print(num, "X", i, "=", (num*i))

# Summation of the list
numbers = [1,2,3,4,5,6,7,8,9]
totalsum = sum(numbers)
print(totalsum)

num1 = int(input("Enter first number:")) # input function takes the value as string so ut has to be changed in integer
num2 = int(input("Enter second number:"))
print(num1+num2)
# or
print(int(num1)+int(num2))

num3 = float(input("Enter first decimal number:")) #input function takes the value as string so it has to be changed in float
num4 = float(input("Enter second decimal number:"))
print(num3+num4)
 # or
print(float(num3)+float(num4))

# find the sum of series
n = int(input("Enter the number: "))
sum = 0
for i in range(1, n+1):
    sum = sum+i
print("The sum of first",n, "series is", sum,".")

# Find cube of the list
n = int(input("Enter the number: "))
cube = n*n*n
print("Cube of {0} is {1} ".format(n, cube))