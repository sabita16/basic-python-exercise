# printing the product
print(5*10)

# data types. creating variable and storing string in it
name = 'John'
names = ['John', 'Auri', 'Deepak']
contacts = {
    'John': 2983,
    'Auri': 2019,
    'Deepak': 4628}
# printing particular name
print(contacts['Auri'])
# Running the python code from terminal using input function

client = input("Whats is the client name: ")
print(client, contacts[client])

# using if statement
client = input("Whats is the client name: ")
if client in contacts:
    print(client, contacts[client])
else:
    print('Sorry, client name not found.')

# From the given 'item' dictionary slice out the title and print on terminal
item = {
    'title': 'python 3.10',
    'price': 4.55,
    'pages': 378}

bookname = input('What is the title of the book?')
print(item[bookname])

# def keyword with if, else statement
# to declare the function, def keyword is needed with a function name and parenthesis
# as it is not a variable but a function name. to start a block of code, a colon is placed after
# parenthesis and after that indentation is given.
def auri(goodpart):
    print("Her good parts: ")
    if goodpart:
        print("helping")
        print("co-operative")
        print("active")
        print("IT worker")
    else:
        print("talkative")
        print("loud")
auri(goodpart=True)